const puppeteer = require('puppeteer');
const cheerio = require('cheerio')
const getApk = async (pkg) => {
  try {
  var browser = await puppeteer.launch();
  } catch (e) {
    var browser = await puppeteer.launch({args: [
        '--no-sandbox'
        ]});
    }
  const page = await browser.newPage();
  const URL = 'https://apk.support/download-app/'+pkg;
  await page.goto(URL)
  await page.waitForSelector('#atload > a')
  const $ = cheerio.load(await page.content())
  const result = ($('#atload > a').attr('href'))
  await page.close();
  await browser.close();
  return result
};
const searchApk = async (pkg) => {
  try {
  var browser = await puppeteer.launch();
  } catch (e) {
    var browser = await puppeteer.launch({args: [
        '--no-sandbox'
        ]});
    }
    const page = await browser.newPage();
  const URL = 'https://apk.support/search?q='+pkg;
  await page.goto(URL)
  await page.waitForSelector('#pageapp > div.seo_list > section')
  const $ = cheerio.load(await page.content())
  let html_res = ($('#pageapp > div.seo_list > section > div'))
  let result = []
  for (let i=0;i<html_res.length;i++){
    result.push({title:html_res.eq(i).find('a > div.stitle').text(),url:html_res.eq(i).find('a').attr('href').split('/')[2]})
  }
  await page.close();
  await browser.close();
  return result
};
const screenshot = async (username,ss=false) => {
  console.log('Opening the browser...');
  try {
  var browser = await puppeteer.launch();
  } catch (e) {
    var browser = await puppeteer.launch({args: [
        '--no-sandbox'
        ]});
    }
  const page = await browser.newPage();
  const URL = 'https://saveinsta.app/en1/instagram-story-download/en?q='+username;
  await page.goto(URL);
  if (ss=='yes'){
    console.log("Saving screenshot")
  await page.screenshot({
    path: './screenshot.png',
    fullPage: true,
  })
  }
  await page.waitForSelector('.download-items')
  const $ = cheerio.load(await page.content())
  const result = []
  const rresult = $('#search-result > ul > li')
  for (let i=0;i<rresult.length;i++){
    const html_res = rresult.eq(i).html()
    if (html_res.includes('click_download_video') || html_res.includes('click_download_image')){
    const enc = (rresult.html().match(/(http:\/\/|ftp:\/\/|https:\/\/|www\.)([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:\/~+#-]*[\w@?^=%&\/~+#-])?/g)?.[1].split('?file=')[1])
    result.push(Buffer.from(enc, 'base64').toString('utf-8').split('&name=')[0])  
  }
    }
  await page.close();
  await browser.close();
  console.log('Scraped story for',username);
  return result
};
const https = require('https')
const express = require('express')
const forge = require('node-forge')


;(function main() {
  const server = https.createServer(
    generateX509Certificate([
      { type: 6, value: 'http://localhost' },
      { type: 7, ip: '127.0.0.1' }
    ]), 
    makeExpressApp()
  )
  server.listen(8080, () => {
    console.log('Listening on 8080')
  })
})()


function generateX509Certificate(altNames) {
  const issuer = [
    { name: 'commonName', value: 'souravkl11.xyz' },
    { name: 'organizationName', value: 'Raganork' },
    { name: 'organizationalUnitName', value: 'Washington Township Plant' }
  ]
  const certificateExtensions = [
    { name: 'basicConstraints', cA: true },
    { name: 'keyUsage', keyCertSign: true, digitalSignature: true, nonRepudiation: true, keyEncipherment: true, dataEncipherment: true },
    { name: 'extKeyUsage', serverAuth: true, clientAuth: true, codeSigning: true, emailProtection: true, timeStamping: true },
    { name: 'nsCertType', client: true, server: true, email: true, objsign: true, sslCA: true, emailCA: true, objCA: true },
    { name: 'subjectAltName', altNames },
    { name: 'subjectKeyIdentifier' }
  ]
  const keys = forge.pki.rsa.generateKeyPair(2048)
  const cert = forge.pki.createCertificate()
  cert.validity.notBefore = new Date()
  cert.validity.notAfter = new Date()
  cert.validity.notAfter.setFullYear(cert.validity.notBefore.getFullYear() + 1)
  cert.publicKey = keys.publicKey
  cert.setSubject(issuer)
  cert.setIssuer(issuer)
  cert.setExtensions(certificateExtensions)
  cert.sign(keys.privateKey)
  return {
    key: forge.pki.privateKeyToPem(keys.privateKey),
    cert: forge.pki.certificateToPem(cert)
  }
}


function makeExpressApp() {
  const app = express()
  app.get('/get', async (req,res)=>{
  try {
  if (!req.query.username) return res.status(404).send('Not found')
  console.log('Looking for stories:',req.query.username)
  let result = await screenshot(req.query.username,req.query.ss)
  return res.json(result||[])  
} catch (error) {
  return res.json({status:false,error:error.message})  
  }

})
  app.get('/apk-search', async (req,res)=>{
  try {
  if (!req.query.id) return res.status(404).send('Not found')
  let result = await searchApk(req.query.id)
  return res.json(result||[])  
} catch (error) {
  return res.json({status:false,error:error.message})  
  }

})
  app.get('/apk-download', async (req,res)=>{
  try {
  if (!req.query.id) return res.status(404).send('Not found')
  let result = await getApk(req.query.id)
  return res.json(result||[])  
} catch (error) {
  return res.json({status:false,error:error.message})  
  }

})
app.get('/get_ss', async (req,res)=>{
  try {
  return res.sendFile(__dirname+'/screenshot.png')
  } catch (error) {
  return res.json({status:false,error:error.message})  
  }

})
app.use(function (req, res) {
  res.status(404).end('Not found');
});

  return app
}
